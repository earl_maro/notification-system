@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}         

                </div>

            
<div class="jumbotron">
  <h1 class="display-4" style="color:#F40505"> Hello!</h1>
  <p class="lead">Welcome to PushNotification.test, click this  <button class="btn btn-info" id="enable-notifications" onclick="enableNotifications()"> Enable Push Notifications </button> to activate push notifications and get notifified when a new post is created </p>
 
  <hr class="my-4">
  <h3 class= "text-center" style="color:#60C236">Post a New Story Today!!</h3>
  <!-- <form action="" method="POST"> -->
  <!-- @csrf -->
<div class="form-group">
    <label for="">Post Title</label>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <input type="text" class="form-control" id="title" name="topic_title" placeholder="" required>
  </div>


<div class="form-group">
    <label for="message">Message</label>
  <textarea class="form-control" autocomplete="off" id="body" name="message" rows="3" placeholder="type a message" required></textarea>
</div>
 <!-- <button type="submit " class="btn btn-primary">Submit</button> -->
 <button class="btn btn-info" id="enable-notifications" onclick="sendNotification()"> Create Post </button>


</div>

<!-- </form> -->
 
</div>


               
            </div>
        </div>
    </div>
</div>



<script>
function sendNotification(){
  var data = new FormData();
data.append('title', document.getElementById('title').value);
data.append('body', document.getElementById('body').value);

var xhr = new XMLHttpRequest();
xhr.open('POST', "{{url('/api/send-notification/'.auth()->user()->id)}}", true);
xhr.onload = function () {
    // do something to response
    console.log(this.responseText);
};
xhr.send(data);
}
var _registration = null;
function registerServiceWorker() {
  return navigator.serviceWorker.register('js/service-worker.js')
  .then(function(registration) {
    console.log('Service worker successfully registered.');
    _registration = registration;
    return registration;
  })
  .catch(function(err) {
    console.error('Unable to register service worker.', err);
  });
}

function askPermission() {
  return new Promise(function(resolve, reject) {
    const permissionResult = Notification.requestPermission(function(result) {
      resolve(result);
    });

    if (permissionResult) {
      permissionResult.then(resolve, reject);
    }
  })
  .then(function(permissionResult) {
    if (permissionResult !== 'granted') {
      throw new Error('We weren\'t granted permission.');
    }
    else{
      subscribeUserToPush();
    }
  });
}

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

function getSWRegistration(){
  var promise = new Promise(function(resolve, reject) {
  // do a thing, possibly async, then…

  if (_registration != null) {
    resolve(_registration);
  }
  else {
    reject(Error("It broke"));
  }
  });
  return promise;
}

function subscribeUserToPush() {
  getSWRegistration()
  .then(function(registration) {
    console.log(registration);
    const subscribeOptions = {
      userVisibleOnly: true,
      applicationServerKey: urlBase64ToUint8Array(
        "{{env('VAPID_PUBLIC_KEY')}}"
      )
    };

    return registration.pushManager.subscribe(subscribeOptions);
  })
  .then(function(pushSubscription) {
    console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
    sendSubscriptionToBackEnd(pushSubscription);
    return pushSubscription;
  });
}

function sendSubscriptionToBackEnd(subscription) {
  return fetch('/api/save-subscription/{{Auth::user()->id}}', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(subscription)
  })
  .then(function(response) {
    console.log(response)

    if (!response.ok) {
      throw new Error('Bad status code from server.');
    }

    return response.json();
  })
  .then(function(responseData) {
    // console.log(responseData)

    if (!(responseData.data && responseData.data.success)) {
    //   throw new Error('Bad response from server.');
    }
  });
}


function enableNotifications(){
  //register service worker
  //check permission for notification/ask
  askPermission();
}
registerServiceWorker();
</script>
@endsection
